const images = ['0.jpeg', '1.jpeg', '2.jpeg', '3.jpeg', '4.jpeg', '5.jpeg', '6.jpeg'];
const slider = document.getElementById('slider');
const picture = slider.querySelector('img');
let check = 0;
let timer;

const baseURL = "/assets/images/"
picture.src = baseURL + images[0];

getTimer();

function addNextPicture() {

    if (check < 6) {
        check++;
    } else {
        check = 0
    }
    picture.src = baseURL + images[check];

    if (check === images.length) {
        check = -1;
    }
}

function getTimer() {
    clearInterval(timer)
    timer = setInterval(addNextPicture, 3000);
}

document.getElementById('next').addEventListener('click', function () {
    addNextPicture()
    getTimer();
});
document.getElementById('prev').addEventListener('click', function () {

    check = --check

    if (check >= 0) {
        picture.src = `${baseURL}${images[check]}`;
    } else {
        check = 6
        picture.src = `${baseURL}${images[6]}`;
    }

    if (check <= 0) {
        check = 7;
    }
    getTimer();
});